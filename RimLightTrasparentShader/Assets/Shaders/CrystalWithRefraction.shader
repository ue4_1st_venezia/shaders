Shader "Unlit/CrystalWithRefraction"
{
    Properties
    {
        _MainTex ("Main Texture", 2D) = "white" {}
        _SecondTex ("Second Texture", 2D) = "white" {}
        _ThirdTex ("Third Texture", 2D) = "white" {}
        _ShineStrenght ("Shine Strenght", Range(0,2)) = 1
        _RimPower ("Rim Power" , Range (0,1)) = 0.5
        _RimWidth("Rim Width", Range (0,1)) = 0.5        
        _RimColor ("Rim Color", Color) = (1,1,1,1)
        _OuterRimColor ("Outer Rim Color", Color) = (1,1,1,1)
    }
    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Transparent"}
        
        GrabPass { "_GrabTex" }

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag       

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float2 uv : TEXCOORD0;
                float3 worldPos : TEXCOORD1;
                float4 screenPosition : TEXCOORD2;
                float3 worldNormal : TEXCOORD3;
                float3 screenNormal : TEXCOORD4;
                float3 viewDir : TEXCOORD5;
                float3 normal : NORMAL;
            };

            sampler2D _GrabTex;
            sampler2D _MainTex;
            float4 _MainTex_ST;
            sampler2D _SecondTex;
            sampler2D _ThirdTex;
            float _ShineStrenght;
            float _RimPower;
            float _RimWidth;      
            float4 _RimColor;
            float4 _OuterRimColor;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                float4 tempScreenPos = ComputeScreenPos(o.vertex);
                float2 textureCoord = tempScreenPos.xy / tempScreenPos.w;
                o.screenPosition = float4(textureCoord, 0.1,0);
                o.worldPos = mul(unity_ObjectToWorld, v.vertex);
                o.viewDir = normalize(o.worldPos - _WorldSpaceCameraPos.xyz);
                o.normal = v.normal;
                o.worldNormal = UnityObjectToWorldNormal(o.normal);
                o.screenNormal = normalize(UnityObjectToViewPos(v.normal));
                o.uv = ComputeGrabScreenPos(o.vertex);
                
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float4 main = tex2D(_MainTex, i.uv);
                float4 second = tex2D(_SecondTex, i.uv);
                float4 col;
                col.rgb = main.rgb;
                col.a = second.a;
                col *= _RimColor;                
                float4 third = tex2D(_ThirdTex, i.uv);
                float2 refraction = i.screenPosition.xy;
                float4 refract = tex2D(_GrabTex, refraction) + col;                
                float4 base = third * (col + _ShineStrenght);
                float rim = 1 + dot(i.viewDir, i.worldNormal);
                float outerRim = pow(rim, _RimPower);
                float3 rimCol = outerRim * _RimWidth * _OuterRimColor;

                return base +float4(rimCol, 1) + refract;
            }
            ENDCG
        }
    }
}
