Shader "Unlit/Crystal_02"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _SecondTex ("Second Texture", 2D) = "white" {}
        _ThirdTex ("Third Texture", 2D) = "white"{}
        _RimColor("Rim Color", Color) = (1,1,1,1)
        _OuterRimColor("OuterRimColor", Color) = (1,1,1,1)
        _RimPower ("Rim Power", Range(0,7)) = 2
        _RimWidth ("Rim Width", Range (0,1)) = 0.5
    }
    SubShader
    {
        Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}
       
        Pass
        {
            Cull Off
            Zwrite Off              
            Blend DstColor Zero

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag           

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float3 worldPos : TEXCOORD1;
                float4 vertex : SV_POSITION;
                float3 viewDir : TEXCOORD2;
                float3 normal : NORMAL;   
                float4 screenPosition : TEXCOORD3;
            };

            sampler2D _MainTex;            
            float4 _MainTex_ST;  
            sampler2D _SecondTex;
            sampler2D _ThirdTex;
            float4 _RimColor;
            float4 _OuterRimColor;      
            float _RimPower;
            float _RimWidth;         

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);              
                o.screenPosition = ComputeScreenPos(o.vertex);
                o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                o.viewDir = normalize(_WorldSpaceCameraPos - mul(unity_ObjectToWorld, v.vertex).xyz);
                o.normal = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);              
                
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {                
                float2 textureCoord = (i.screenPosition.xy / i.screenPosition.w);              
                float4 col = tex2D(_MainTex, textureCoord);
                float4 secondCol = tex2D(_SecondTex, textureCoord);
                float4 thirdCol = tex2D(_ThirdTex, i.uv);          
                float rim = 1 - abs(dot(i.viewDir, i.normal));                
                float outerRim = pow(rim, 4);
                rim = pow(rim, _RimWidth);
                rim = pow(rim, _RimPower);

                col.rgb += rim * _RimColor;
                col.rgb += outerRim + _OuterRimColor;
                secondCol.rgb += rim * _RimColor;
                secondCol.rgb += rim * _OuterRimColor;
                float4 finalCol = lerp(col, secondCol, thirdCol);
                return finalCol;
            }
            ENDCG
        }
    }
}
