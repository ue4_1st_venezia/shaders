Shader "Unlit/CrystalWithGrabPass"
{
	Properties
	{
		_RimColor("Rim Color", Color) = (1,1,1,1)
		_OuterRimColor("OuterRimColor", Color) = (1,1,1,1)
		_RimPower("Rim Power", Range(0,7)) = 2
		_RimWidth("Rim Width", Range(0,1)) = 0.5
	}
		SubShader
	{
		Tags { "RenderType" = "Opaque" "Queue" = "Transparent"}

		GrabPass{  "_BGTexture"}

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag           

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
				float3 normal : NORMAL;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;

				float4 vertex : SV_POSITION;
				float4 screenPosition : TEXCOORD2;
				float3 viewDir : TEXCOORD3;
				float3 normal : NORMAL;
			};

			sampler2D _BGTexture;
			float4 _RimColor;
			float4 _OuterRimColor;
			float _RimPower;
			float _RimWidth;

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);

				o.screenPosition = ComputeGrabScreenPos(o.vertex);
				o.viewDir = normalize(_WorldSpaceCameraPos - mul(unity_ObjectToWorld, v.vertex).xyz);
				o.normal = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);

				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				float4 bgCol = tex2Dproj(_BGTexture, i.screenPosition);
				//float2 textureCoord = (i.screenPosition.xy / i.screenPosition.w);
				//float4 bgCol = tex2D(_BGTexture, textureCoord);					
				float rim = 1 - abs(dot(i.viewDir, i.normal));
				float outerRim = pow(rim, 4);
				rim = pow(rim, _RimWidth);
				rim = pow(rim, _RimPower);

				bgCol.rgb += rim * _RimColor;
				bgCol.rgb += outerRim + _OuterRimColor;
				return bgCol;
			 }
			 ENDCG
		 }
	}
}
