Shader "Unlit/Crystal_01"
{
    Properties
    {
        _RimColor1("Rim Color 1", Color) = (1,1,1,1)
        _RimColor2("Rim Color 2", Color) = (1,1,1,1)
        _RimPower("RimPower", Range(0,5)) = 1
        _RimWidth("RimWidth", Range(0,1)) = 0.5
    }

    SubShader
    {
        Tags { "RenderType"="Transparent" "Queue" = "Transparent"}
       
        Pass
        {
            Cull Off
            Zwrite Off
            Blend DstColor Zero

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
      
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float3 normal : NORMAL;
                float3 viewDir : TEXCOORD1;
            };

            float4 _RimColor1;
            float4 _RimColor2;
            float _RimPower;
            float _RimWidth;

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.viewDir = normalize(_WorldSpaceCameraPos - mul(unity_ObjectToWorld, v.vertex).xyz);
                o.normal = normalize(mul(float4(v.normal, 0), unity_WorldToObject).xyz);
             
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float rim = 1 - abs(dot(i.viewDir, i.normal));
                float rim2 = pow(rim, 4);
                rim = pow(rim, _RimWidth);
                rim = pow(rim, _RimPower);
                float4 col = (i.uv,0,0);
                col.rgb += rim * _RimColor1;
                col.rgb += rim2 + _RimColor2;               
                return col;
            }
            ENDCG
        }
    }
}
