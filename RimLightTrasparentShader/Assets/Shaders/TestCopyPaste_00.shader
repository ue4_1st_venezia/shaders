Shader "Unlit/TestCopyPaste_00"
{
    Properties
    {
        _MainTex("Texture", 2D) = "white" {}
        _RimColor("Rim Color", Color) = (1,1,1,1)
        _OuterRimColor("OuterRimColor", Color) = (1,1,1,1)
        _RimPower("Rim Power", Range(0,7)) = 2
        _RimWidth("Rim Width", Range(0,1)) = 0.5
        _NormalMap("Normal Map", 2D) = "bump" {}
        _NormalStrength("Normal Sthrength", Range(0,1)) = 1
        _NormalTiling("Normal Tiling", Vector) = (0,0,0,0)
        _NormalOffset("Normal Offset", Vector) = (0,0,0,0)
    }
        SubShader
        {
            Tags { "RenderType" = "Transparent" "Queue" = "Transparent"}

            Pass
            {
                Cull Off
                Zwrite Off
                Blend DstColor Zero

                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag           

                #include "UnityCG.cginc"

                struct appdata
                {
                    float4 vertex : POSITION;
                    float2 uv : TEXCOORD0;
                    float3 normal : NORMAL;
                    
                };

                struct v2f
                {
                    float2 uv : TEXCOORD0;
                    float2 uv2 : TEXCOORD1;
                    float4 vertex : SV_POSITION;
                    float4 screenPosition : TEXCOORD2;
                    float3 viewDir : TEXCOORD3;
                    float3 normal : NORMAL;
                };

                sampler2D _MainTex;
                float4 _MainTex_ST;
                float4 _RimColor;
                float4 _OuterRimColor;
                float _RimPower;
                float _RimWidth;
                sampler2D _NormalMap;
                float4 _NormalMap_ST;
                float _NormalStrength;
                float2 _NormalTiling;
                float2 _NormalOffset;

                v2f vert(appdata v)
                {
                    v2f o;
                    o.vertex = UnityObjectToClipPos(v.vertex);
                    o.uv = TRANSFORM_TEX(v.uv, _MainTex);
                    o.uv2 = TRANSFORM_TEX(v.uv, _NormalMap);
                    o.screenPosition = ComputeScreenPos(o.vertex);
                    o.viewDir = normalize(_WorldSpaceCameraPos - mul(unity_ObjectToWorld, v.vertex).xyz);
                    o.normal = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);

                    return o;
                }

                fixed4 frag(v2f i) : SV_Target
                {
                    float2 uvModifier = UnpackNormal(tex2D(_NormalMap, (i.uv2 * _NormalTiling) + (_NormalOffset))) * _NormalStrength;
                    float2 textureCoord = (i.screenPosition.xy / i.screenPosition.w) + uvModifier;                  
                    float4 col = tex2D(_MainTex, textureCoord);
                    float rim = 1 - abs(dot(i.viewDir, i.normal));
                    float outerRim = pow(rim, 4);
                    rim = pow(rim, _RimWidth);
                    rim = pow(rim, _RimPower);

                    col.rgb += rim * _RimColor;
                    col.rgb += outerRim + _OuterRimColor;
                    return col;
                }
                ENDCG
        }
    }
}
