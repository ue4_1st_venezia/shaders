Shader "Custom/CrystalTest"
{
    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Smoothness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" } //da rendere transparent con anche la queue transparent
        LOD 200

        CGPROGRAM
        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
        };

        half _Smoothness;
        half _Metallic;
        fixed4 _Color;

        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Smoothness;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}

// _Color (gi� c'�; Cambiare il nome con MainColor)
// _Metallic (gi� c'�)
// _Smoothness (gi� c'�)
// cercare il freanel effect
// creare l'_Emission (a menoi che non sia il glossiness)
// creare un secondo colore (_TopColor)
// creare un terzo colore (_BottomColor)
// moltiplicare il _TopColor con il _FreanelEffect e collegarlo all'emission (facoltativo dato che non voglio un qualcosa di emittente)
// splittare le UV con il canale verde (green) e moltiplicarlo per un colore a caso e usare il smoothstep
// il multiplty  portarlo anche su un OneMinus e anceh qua usare il smoothstep per separare i gradienti
// creare altri due range per il _TopLine e il _BottomLine
// mettere il top

