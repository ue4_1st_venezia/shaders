Shader "Unlit/RimLightNoTex"
{
    Properties
    {
        _NormalStrength("Normal Sthrength", Range(0,1)) = 1
        _NormalTiling("Normal Tiling", Vector) = (0,0,0,0)
        _NormalOffset("Normal Offset", Vector) = (0,0,0,0)
        _RimColor("Rim Color", Color) = (1,1,1,1)
        _OuterRimColor("OuterRimColor", Color) = (1,1,1,1)
        [PowerSlider(2)] _RimWidth("Rim Width", Range(0.5,3)) = 2
    }
        SubShader
        {
            
            Tags { "RenderType" = "Transparent" "Queue" = "transparent" }
            Blend SrcAlpha OneMinusSrcAlpha
            LOD 100

            Pass
            {
                Cull Off
                Zwrite Off
                Blend DstColor Zero

                CGPROGRAM
                #pragma vertex vert
                #pragma fragment frag
            // make fog work
            #pragma multi_compile_fog

            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
                float3 normal : NORMAL;

            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float2 uv2 : TEXCOORD1;
              
                float4 vertex : SV_POSITION;
                float4 screenPosition : TEXCOORD2;
                float3 viewDir : TEXCOORD3;
                float3 normal : NORMAL;
            };

            float _NormalStrength;
            float2 _NormalTiling;
            float2 _NormalOffset;
            float4 _RimColor;
            float4 _OuterRimColor;           
            float _RimWidth;

            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);                
                o.screenPosition = ComputeScreenPos(o.vertex);

                o.viewDir = normalize(_WorldSpaceCameraPos - mul(unity_ObjectToWorld, v.vertex).xyz);
                o.normal = normalize(mul(float4(v.normal, 0.0), unity_WorldToObject).xyz);
                UNITY_TRANSFER_FOG(o,o.vertex);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float2 textureCoord = (i.screenPosition.xy / i.screenPosition.w);
                float rim = 1 - abs(dot(i.viewDir, i.normal));
                float outerRim = pow(rim, 4);
                rim = pow(rim, _RimWidth);

                float4 col = (i.uv, 0, 0);
                col.rgb += rim * _RimColor;
                col.rgb += outerRim + _OuterRimColor;

                return col;
            }
            ENDCG
        }
        }
}
